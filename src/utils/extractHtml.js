const getUrlAsDOM = (url) => {
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", 'https://cors-anywhere.herokuapp.com/' + url, false);
    xmlHttp.send();
    const parser = new DOMParser();
    return parser.parseFromString(xmlHttp.responseText, "text/html");
}


export {
    getUrlAsDOM,
}