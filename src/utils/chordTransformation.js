import chordsScale from './chordsScale';

const wordDelimiters = ["add", "aug", "dim", "maj", "sus"];
const wordDelimitersRegex = new RegExp(wordDelimiters.join("|"));
const digitDelimiters = ["4", "5", "6", "7", "8"];
const digitDelimitersRegex = new RegExp(digitDelimiters.join("|"));
const charDelimiters = ["m"];
const charDelimitersRegex = new RegExp(charDelimiters.join("|"));

const cleanFromDelimiters = (str, regex) => {
    let newStr = str;
    const delimiterIndex = str.search(regex);
    if (delimiterIndex >= 0) {
        newStr = str.slice(0, delimiterIndex);
    }
    return {
        str: newStr,
        delimiterIndex,
    };
}

const updateChordData = (oldData, newData) => {
    if (newData.delimiterIndex !== -1) {
        oldData.cleanChord = newData.str;
        oldData.delimiterStartIndex = newData.delimiterIndex;
    }
}

const transformChord = (chord, skips) => {
    if (chord.includes('/')) {
        const splitted = chord.split('/');
        console.log(splitted, "splitted")
        return updateChord(splitted[0], skips) + '/' + updateChord(splitted[1], skips)
    }
    return updateChord(chord, skips)
}

const updateChord = (chord, skips) => {
    console.log(chord)
    let chordData = {
        cleanChord: chord,
        delimiterStartIndex: -1,
    };
    let tempData;

    tempData = cleanFromDelimiters(
        chordData.cleanChord,
        wordDelimitersRegex
    )
    updateChordData(chordData, tempData);

    tempData = cleanFromDelimiters(
        chordData.cleanChord,
        digitDelimitersRegex
    )
    updateChordData(chordData, tempData);

    tempData = cleanFromDelimiters(
        chordData.cleanChord,
        charDelimitersRegex
    )
    updateChordData(chordData, tempData);
    console.log(chordData)

    console.log(changeChordTone(chordData.cleanChord, skips))
    console.log(changeChordTone(chordData.cleanChord, skips) + chord.slice(chordData.delimiterStartIndex));
    return changeChordTone(chordData.cleanChord, skips) + (chordData.delimiterStartIndex === -1 ? "" : chord.slice(chordData.delimiterStartIndex))
};

const currentChordIndex = (chordName) => {
    for (let chordIndex = 0; chordIndex < chordsScale.length; chordIndex++) {
        if (chordsScale[chordIndex].names.includes(chordName)) {
            return chordIndex;
        }
    }
    return -1;
}

const newChordIndex = (currentIndex, toneSkips) => {
    if (currentIndex + toneSkips >= chordsScale.length) {
        return (currentIndex + toneSkips) % chordsScale.length
    }
    if (currentIndex + toneSkips < 0) {
        return chordsScale.length + currentIndex + toneSkips;
    }
    console.log(currentIndex + toneSkips)
    return currentIndex + toneSkips;
}

const changeChordTone = (chord, toneSkips) => {
    let currentIndex = currentChordIndex(chord);
    let newIndex = newChordIndex(currentIndex, toneSkips);
    return chordsScale[newIndex].mainName;
}

export {
    transformChord,
}