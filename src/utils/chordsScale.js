const arrayList =  [
    {
        mainName: "A",
        names: ["A"],
    },
    {
        mainName: "Bb",
        names: ["Bb", "A#"],
    },
    {
        mainName: "B",
        names: ["B"],
    },
    {
        mainName: "C",
        names: ["C"],
    },
    {
        mainName: "C#",
        names: ["C#", "Db"],
    },
    {
        mainName: "D",
        names: ["D"],
    },
    {
        mainName: "Eb",
        names: ["Eb", "D#"],
    },
    {
        mainName: "E",
        names: ["E"],
    },
    {
        mainName: "F",
        names: ["F"],
    },
    {
        mainName: "F#",
        names: ["F#", "Gb"],
    },
    {
        mainName: "G",
        names: ["G"],
    },
    {
        mainName: "Ab",
        names: ["Ab", "G#"],
    },
];

export default arrayList;