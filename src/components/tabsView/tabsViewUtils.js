import React from 'react';
import { Parser as HtmlToReactParser} from 'html-to-react';

const extractSongRawHtmlContent = (dom) => {
    return dom.getElementById("songContentTPL");
}

const rawHtmlToReactElements = (songContent) => {
    const parser = new HtmlToReactParser();
    return parser.parse(songContent.outerHTML)
}

export {
    extractSongRawHtmlContent,
    rawHtmlToReactElements
}