import React, { useState, useEffect } from 'react';
import { rawHtmlToReactElements } from "./tabsViewUtils";
import { transformChord } from "../../utils/chordTransformation";

const tones = [
    {
        value: -5,
        text: "-2.5"
    },
    {
        value: -4,
        text: "-2"
    },
    {
        value: -3,
        text: "-1.5"
    },
    {
        value: -2,
        text: "-1"
    },
    {
        value: -1,
        text: "-0.5"
    },
    {
        value: 0,
        text: "0"
    },
    {
        value: 1,
        text: "0.5"
    },
    {
        value: 2,
        text: "1"
    },
    {
        value: 3,
        text: "1.5"
    },
    {
        value: 4,
        text: "2"
    },
    {
        value: 5,
        text: "2.5"
    },
]

const extractDeepestChild = (parentNode, selector) => {
    let lastNode = parentNode;
    while (lastNode.querySelectorAll(selector).length !== 0) {
        lastNode = lastNode.querySelector(selector);
    }
    return lastNode
}

const TabsView = (props) => {
    const [songContent, setSongContent] = useState(null);
    const [tone, setTone] = useState(0);

    useEffect(() => {
        setSongContent(props.originalDataContent);
        setTone(0);
        console.log("URLchange")
    }, [props.originalDataContent]);

    const handleToneChange = (event) => {
        setTone(Number(event.target.value))
    }

    const calcSongContent = () => {
        let a = props.originalDataContent.cloneNode(true);
        const nodes = [...a.querySelectorAll(".chords > span")];
        nodes.forEach((chordNode) => {
            let nettoChordNode = extractDeepestChild(chordNode, "span");
            console.log(nettoChordNode, "beforeChange");
            if (nettoChordNode.innerText !== "undefined") {
                nettoChordNode.textContent = transformChord(nettoChordNode.textContent, tone)
                console.log(nettoChordNode, "afterChange");
            }
        })
        console.log(a, "ddd")
        setSongContent(a)
        return a;
    }

    useEffect(() => {
        if (props.originalDataContent) {
            if (tone !== 0) {
                calcSongContent()
            } else {
                console.log(props.originalDataContent, "original")
                setSongContent(props.originalDataContent)
                console.log("done")
            }
        }
    }, [tone]);

    return (
        <>
        <select className="tone-select"
                value={tone}
                onChange={handleToneChange}
                disabled={!props.originalDataContent}
        >
            {tones.map(tone => (
                <option key={tone.value} value={tone.value}>{tone.text}</option>
            ))}
        </select>
        <div className="tabs">
            {songContent &&
            <div className="stanza">
                {rawHtmlToReactElements(songContent)}
                <br/>
            </div>
            }
        </div>
        </>
    )
};



export default TabsView;