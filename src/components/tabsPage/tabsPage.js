import React, {useEffect, useState} from 'react';
import SearchPanel from "../searchPanel/searchPanel";
import TabsView from "../tabsView/tabsView";
import {extractSongRawHtmlContent} from "../tabsView/tabsViewUtils";
import {getUrlAsDOM} from "../../utils/extractHtml";

const TabsPage = () => {
    const [url, setUrl] = useState("https://www.tab4u.com/tabs/songs/407_%D7%90%D7%99%D7%99%D7%9C_%D7%92%D7%95%D7%9C%D7%9F_-_%D7%95%D7%90%D7%A0%D7%99_%D7%A7%D7%95%D7%A8%D7%90_%D7%9C%D7%9A.html");
    const [originalData, setOriginalData] = useState(null)

    const handleUrlChange = (newUrl) => {
        setUrl(newUrl)
    }

    useEffect(() => {
        const extractedSongContent = extractSongRawHtmlContent(getUrlAsDOM(url));
        setOriginalData(extractedSongContent)
    }, [url]);

    return (
        <div className="tabs-page">
            <SearchPanel url={url}
                         onUrlChange={handleUrlChange}
            />
            <TabsView url={url}
                      originalDataContent={originalData}
            />
        </div>
    )
}

export default TabsPage;