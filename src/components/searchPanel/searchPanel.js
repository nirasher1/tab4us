import React, { useState } from 'react';

const SearchPanel = (props) => {
    const [url, setUrl] = useState(props.url)

    const handleUrlChange = (event) => {
        setUrl(event.target.value);
    }

    const handleUrlSubmit = () => {
        props.onUrlChange(url);
    }

    const handleUrlPasteSubmit = async () => {
        const clipboardText = await navigator.clipboard.readText();
        props.onUrlChange(clipboardText);
    }

    return (
        <div className="search-panel">
            <input value={url}
                   onChange={handleUrlChange}
            />
            <button onClick={handleUrlSubmit}
            >
                חפש
            </button>
            <button onClick={handleUrlPasteSubmit}
            >
                הדבק וחפש
            </button>
        </div>
    )
}

export default SearchPanel;