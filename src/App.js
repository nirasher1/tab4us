import React from 'react';
import './App.css';
import TabsPage from "./components/tabsPage/tabsPage";

function App() {
  return (
    <div className="App">
        <TabsPage id="tabs-view"/>
    </div>
  );
}

export default App;
